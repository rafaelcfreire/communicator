package com.br.ehnois.ejb;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class Calculator
 */
@Stateless
@LocalBean
public class Calculator implements CalculatorRemote, CalculatorLocal {

    /**
     * Default constructor. 
     */
    public Calculator() {
        // TODO Auto-generated constructor stub
    }

    public int calcula(){
    	return 6 + 1;
    }
}
