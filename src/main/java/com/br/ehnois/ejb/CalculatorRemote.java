package com.br.ehnois.ejb;

import javax.ejb.Remote;

@Remote
public interface CalculatorRemote {
	public int calcula();
}
