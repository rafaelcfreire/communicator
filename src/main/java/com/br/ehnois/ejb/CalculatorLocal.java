package com.br.ehnois.ejb;

import javax.ejb.Local;

@Local
public interface CalculatorLocal {
	public int calcula();
}
